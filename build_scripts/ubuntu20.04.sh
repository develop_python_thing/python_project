#!/bin/bash

set -xe

python3.8 setup.py build && \
    python3.8 -m unittest discover tests/ '*test.py'

mkdir -p python3-wikipedia-1.1-1/usr/lib/python3/dist-packages
mv build/lib/* python3-wikipedia-1.1-1/usr/lib/python3/dist-packages
mv DEBIAN python3-wikipedia-1.1-1
dpkg-deb --build python3-wikipedia-1.1-1
